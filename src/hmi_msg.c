#include "hmi_msg.h"


const char ask_number_between[] PROGMEM =
    "Please enter number between 0 and 9!\r\n";
const char not_number[] PROGMEM = "Not a number";
const char not_valid[] PROGMEM = "Not valid number";

const char number_1[] PROGMEM = "Zero";
const char number_2[] PROGMEM = "One";
const char number_3[] PROGMEM = "Two";
const char number_4[] PROGMEM = "Three";
const char number_5[] PROGMEM = "Four";
const char number_6[] PROGMEM = "Five";
const char number_7[] PROGMEM = "Six";
const char number_8[] PROGMEM = "Seven";
const char number_9[] PROGMEM = "Eight";
const char number_10[] PROGMEM = "Nine";

PGM_P const number_table[] PROGMEM = {
    number_1,
    number_2,
    number_3,
    number_4,
    number_5,
    number_6,
    number_7,
    number_8,
    number_9,
    number_10
};
