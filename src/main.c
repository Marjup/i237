#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/atomic.h>
#include "hmi_msg.h"
#include "print_helper.h"
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "../lib/helius_microrl/microrl.h"
#include "cli_microrl.h"


#define UART_BAUD 9600
#define UART_STATUS_MASK 0x00FF


/* Global seconds counter */
volatile uint32_t counter;

/* Create microrl object and pointer on it */
microrl_t rl;
microrl_t *prl = &rl;


/* Initial setup for uart 1 */
static inline void init_uart(void)
{
    uart1_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart1_puts_p(PSTR(VER_FW));
    uart1_puts_p(PSTR(VER_LIBC));
}


/* Initial setup for uart 0 and microrl */
static inline void init_micro(void)
{
    uart0_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart0_puts_p(PSTR(STUDENT_NAME "\r\n"));
    uart0_puts_p(PSTR("Use backspace to delete entry and enter to confirm.\r\n"));
    uart0_puts_p(PSTR("Arrow key's and del do not work currently.\r\n"));
    microrl_init(prl, uart0_puts);
    microrl_set_execute_callback(prl, cli_execute);
}


/* Initial setup for LCD */
static inline void init_LCD(void)
{
    lcd_init();
    lcd_home();
    lcd_puts_P(PSTR(STUDENT_NAME_LCD));
}


/* Intial setup for seconds counter */
static inline void init_counter(void)
{
    counter = 0;
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1B |= _BV(WGM12);
    TCCR1B |= _BV(CS12);
    OCR1A = 62549;
    TIMSK1 |= _BV(OCIE1A);
}


/* Heartbeat for counting system worktime */
static inline void heartbeat(void)
{
    uint32_t counter_cpy = 0;
    static uint32_t count;
    char ascii_buf[11] = {0x00};
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        counter_cpy = counter;
    }

    if (count != counter_cpy) {
        ltoa(counter_cpy, ascii_buf, 10);
        uart1_puts_p(PSTR("Uptime: "));
        uart1_puts(ascii_buf);
        uart1_puts_p(PSTR(" s.\r\n"));
        count = counter_cpy;
    }
}


void main (void)
{
    init_uart();
    init_counter();
    init_micro();
    sei();
    init_LCD();

    while (1) {
        heartbeat();
        // CLI command are handled in cli_execute()
        microrl_insert_char(prl, (uart0_getc() & UART_STATUS_MASK));
    }
}


/* Counter 1 ISR */
ISR(TIMER1_COMPA_vect)
{
    counter++;
}
