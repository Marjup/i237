#ifndef HMI_MSG_H
#define HMI_MSG_H
#include <avr/pgmspace.h> 

#define VER_FW   "Version: " FW_VERSION " built on: " __DATE__ " " __TIME__ "\r\n"
#define VER_LIBC "avr-libc version: " __AVR_LIBC_VERSION_STRING__ " avr-gcc version: " __VERSION__ "\r\n"

#define STUDENT_NAME "Marju Pütsepp"
#define STUDENT_NAME_LCD "Marju P" "\xF5" "tsepp"

extern const char ask_number_between[] PROGMEM;
extern const char not_number[] PROGMEM;
extern const char not_valid[] PROGMEM;

extern PGM_P const number_table[] PROGMEM;

#endif
